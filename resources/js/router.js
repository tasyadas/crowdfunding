import Vue    from 'vue'
import Router from 'vue-router'

Vue.use(Router)

//DEFINE ROUTE
const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            alias: '/home',
            component: () => import(/* webpackChunkName: "categories" */ './views/Home')
        },
        {
            path: '/donations',
            name: 'donations',
            component: () => import(/* webpackChunkName: "categories" */ './views/Donation')
        },
        {
            path: '/campaigns',
            name: 'campaigns',
            component: () => import(/* webpackChunkName: "categories" */ './views/Campaigns')
        },
        {
            path: '/campaign/:id',
            name: 'campaign',
            component: () => import(/* webpackChunkName: "categories" */ './views/Campaign')
        },
        {
            path: '/auth/social/:provider/callback',
            name: 'social',
            component: () => import(/* webpackChunkName: "categories" */ './views/Social')
        },
        {
            path: '*',
            redirect: '/'
        }
    ]
})

export default router
