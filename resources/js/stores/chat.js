export default {
    namespaced: true,
    state: {
        chat: {},
    },
    mutations: {
        setChat: (state, chat) => {
            state.chat = chat
        },
    },
    actions: {
        setChat: ({commit}, chat) => {
            commit('setChat', chat)
        },
    },
    getters: {
        chat    : state => state.chat,
    }
}
