@component('mail::message')
    <div>
        Hi, {{ $user->name }}
        <br/><br/>
        @lang('To proceed further with your account in CrowdFunding, please use the following OTP')
        <br/>
        {{$user->otp->otp}}
    </div>
    <br/><br/><br/>
    @lang('Thanks'),<br>{{ config('app.name') }}
@endcomponent
