# CrowdFunding Website View

## Login View
![](public/asset-demo/Login.png)

## Login with Google View
![](public/asset-demo/Login%20with%20google.png)

## Home View
![](public/asset-demo/home.png)

## All Campaign View
![](public/asset-demo/All%20Campaigns.png)

## Campaign Detail View
![](public/asset-demo/Campaign%20Detail.png)

## Search Campaign View
![](public/asset-demo/Search%20Campaign.png)

## Logout View
![](public/asset-demo/Logout.png)

## Apps Demo

[Here is the link to the video](https://drive.google.com/file/d/1BzBlH1lNAYrpfTPKZjmkzPPe0zm3Rc7N/view?usp=sharing)
