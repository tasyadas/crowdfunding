<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Otp;
use App\Models\User;
use Carbon\Carbon;

class OtpCodeController extends Controller
{
    /**
     * @param User $user
     * @return Otp
     */
    public function generator(User $user)
    {
        $newOtp = rand(100000,999999);
        $isExist = Otp::where('otp', $newOtp)->first();

        if ($isExist){
            return $this->generator($user);
        }

        $otp = new Otp([
            'otp'         => $newOtp,
            'expire_date' => Carbon::now()->addMinutes(5)
        ]);

        $otp->user()->associate($user);
        $otp->save();

        return $otp;
    }
}
