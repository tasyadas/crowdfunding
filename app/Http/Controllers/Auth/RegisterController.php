<?php

namespace App\Http\Controllers\Auth;

use App\Events\OTPCodeCreated;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'email' => 'required|unique:users'
        ]);

        $otp = new OtpCodeController();
        $user = new User([
            'name'  => $request->name,
            'email' => $request->email
        ]);

        $user->role()->associate(Role::find('b35d4e28-194c-40da-8a9d-2f5af9f89731'));
        $user->save();

        $otp->generator($user);
        event(new OTPCodeCreated($user));

        return [
            'response_code'    => "00",
            'response_message' => 'silahkan cek email',
            'data'             => $user
        ];
    }
}
