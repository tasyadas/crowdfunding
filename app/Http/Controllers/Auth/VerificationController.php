<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Otp;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    /**
     * @param Request $request
     *
     * @return array|string[]
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'otp' => 'required'
        ]);

        $otp = Otp::where('otp', $request->otp)->first();

        if (!$otp) {
            return [
                'response_code'    => '01',
                'response_message' => 'Kode OTP tidak ditemukan!'
            ];
        }

        $now = Carbon::now();

        if ($now > $otp->expire_date) {
            return [
                'response_code'    => '01',
                'response_message' => 'Kode OTP sudah tidak berlaku, silahkan generate ulang!'
            ];
        }

        $otp->user->email_verified_at = Carbon::now();
        $otp->push();

        $user = $otp->user;
        $otp->delete();

        return [
            'response_code'    => '00',
            'response_message' => 'Akun berhasil terverifikasi!',
            'data'             => $user
        ];
    }
}
