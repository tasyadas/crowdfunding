<?php

namespace App\Http\Controllers\Auth;

use App\Events\OTPCodeCreated;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class RegenerateOtpCodeController extends Controller
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' => 'required|exists:users'
        ]);

        $user = User::where('email', $request->email)->first();
        $user->otp->delete();

        $newOtp = new OtpCodeController();
        $newOtp = $newOtp->generator($user);
        unset($newOtp['user']);
        unset($user['otp']);
        $user['otp'] = $newOtp;

        event(new OTPCodeCreated($user));

        return [
            'response_code'    => "00",
            'response_message' => 'silahkan cek email',
            'data'             => $user
        ];
    }
}
