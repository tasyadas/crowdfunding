<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LogoutController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function __invoke()
    {
        auth()->logout();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'user berhasil logout'
        ], 200);
    }
}
