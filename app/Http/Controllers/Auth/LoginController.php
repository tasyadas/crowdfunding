<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * @param Request $request
     *
     * @return array|Application|ResponseFactory|Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email'    => 'required',
            'password' => 'required'
        ]);

        if (!$token = auth()->attempt($request->only('email', 'password'))) {
            return response(['error' => 'Email atau Password tidak ditemukan'], 401);
        }

        $data['token'] = $token;
        $data['user']  = Auth::user();

        return [
            'response_code'    => "00",
            'response_message' => 'User berhasil login',
            'data'             => $data
        ];
    }
}
