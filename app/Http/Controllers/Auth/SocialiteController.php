<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Laravel\Socialite\Facades\Socialite;

class SocialiteController extends Controller
{
    /**
     * @param $provider
     *
     * @return JsonResponse
     */
    public function redirectToProvider($provider)
    {
        $url = Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();

        return response()->json([
            'url' => $url
        ]);
    }

    public function handleProviderCallback($provider)
    {
        try {
            $socialUser = Socialite::driver($provider)->stateless()->user();

            if (!$socialUser) {
                return response()->json([
                    'response_code'    => '01',
                    'response_message' => 'login failed'
                ], 401);
            }

            $user = User::whereEmail($socialUser->email)->first();

            if (!$user) {
                $user = new User([
                    'email'             => $socialUser->email,
                    'name'              => $socialUser->name,
                    'email_verified_at' => Carbon::now(),
                    'photo_profile'     => $provider === 'google' ?: $socialUser->avatar
                ]);

                $user->role()->associate(Role::find('b35d4e28-194c-40da-8a9d-2f5af9f89731'));
                $user->save();
            }

            $data['user'] = $user;
            $data['token'] = auth()->login($user);

            return response()->json([
                'response_code'    => '00',
                'response_message' => 'user berhasil login',
                'data'             => $data
            ], 200);
        } catch (\Throwable $th) {

            return response()->json([
                'response_code'    => '01',
                'response_message' => 'login failed'
            ], 401);
        }
    }
}
