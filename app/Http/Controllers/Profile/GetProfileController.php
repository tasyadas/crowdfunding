<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GetProfileController extends Controller
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function __invoke(Request $request)
    {
        return [
            'response_code'    => "00",
            'response_message' => 'Profile berhasil ditampilkan',
            'data'             => $request->user()
        ];
    }
}
