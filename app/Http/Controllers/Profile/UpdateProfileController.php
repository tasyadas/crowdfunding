<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;

class UpdateProfileController extends Controller
{
    /**
     * @var ImageManager
     */
    private $image;

    /**
     * ImageUploader constructor.
     * @param ImageManager $image
     */
    public function __construct(ImageManager $image)
    {
        $this->image = $image;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            "name"  => "nullable",
            "photo" => "nullable|mimes:jpeg,bmp,png,jpg"
        ]);

        $user = $request->user();
        $user->name = $request->name ?: $user->name;

        if (null !== $image = $request->photo) {
            $tmpFilePath = $this->getTempPath($image->getClientOriginalExtension());
            $upload = $this->image->make($image->getRealPath())->save($tmpFilePath);

            $filePath = sprintf('profiles/%s/', $user->id) . uniqid() . '.' . $image->getClientOriginalExtension();
            Storage::put('public/'.$filePath, file_get_contents($tmpFilePath));
            $user->photo = $filePath;

            $upload->destroy();
        }

        $user->save();

        return [
            'response_code'    => "00",
            'response_message' => 'Profile berhasil diupdate',
            'data'             => $user
        ];
    }

    /**
     * @param string $extension
     *
     * @return string
     */
    private function getTempPath(string $extension)
    {
        return sys_get_temp_dir() . '/' . uniqid() . '.' . $extension;
    }
}
