<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;

class PickRandomBlogController extends Controller
{
    /**
     * @param $count
     *
     * @return array
     */
    public function __invoke($count)
    {
        $blogs = Blog::select('*')
            ->inRandomOrder()
            ->limit($count)
            ->get();

        return [
            'response_code'    => "00",
            'response_message' => 'Blog berhasil ditampilkan',
            'data'             => $blogs
        ];
    }
}
