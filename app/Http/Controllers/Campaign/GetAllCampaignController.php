<?php

namespace App\Http\Controllers\Campaign;

use App\Http\Controllers\Controller;
use App\Models\Campaign;

class GetAllCampaignController extends Controller
{
    /**
     * @return array
     */
    public function __invoke()
    {
        return [
            'response_code'    => "00",
            'response_message' => 'Campaign berhasil ditampilkan',
            'data'             => Campaign::paginate(6)
        ];
    }
}
