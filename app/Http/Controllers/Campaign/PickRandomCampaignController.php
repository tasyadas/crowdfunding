<?php

namespace App\Http\Controllers\Campaign;

use App\Http\Controllers\Controller;
use App\Models\Campaign;
use Illuminate\Http\Request;

class PickRandomCampaignController extends Controller
{

    /**
     * @param $count
     *
     * @return array
     */
    public function __invoke($count)
    {
        $campaigns = Campaign::select('*')
                    ->inRandomOrder()
                    ->limit($count)
                    ->get();

        return [
            'response_code'    => "00",
            'response_message' => 'Campaign berhasil ditampilkan',
            'data'             => $campaigns
        ];
    }
}
