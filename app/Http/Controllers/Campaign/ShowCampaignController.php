<?php

namespace App\Http\Controllers\Campaign;

use App\Http\Controllers\Controller;
use App\Models\Campaign;

class ShowCampaignController extends Controller
{
    /**
     * @param $campaign
     *
     * @return array
     */
    public function __invoke($campaign)
    {
        return [
            'response_code'    => "00",
            'response_message' => 'Detail Campaign berhasil ditampilkan',
            'data'             => Campaign::find($campaign)
        ];
    }
}
