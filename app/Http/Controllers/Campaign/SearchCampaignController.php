<?php

namespace App\Http\Controllers\Campaign;

use App\Http\Controllers\Controller;
use App\Models\Campaign;

class SearchCampaignController extends Controller
{
    /**
     * @param $keyword
     *
     * @return array
     */
    public function __invoke($keyword)
    {
        $campaign = Campaign::select('*')
                    ->where('title', 'LIKE', '%'.$keyword.'%')
                    ->get();

        return [
            'response_code'    => "00",
            'response_message' => 'Campaign berhasil ditampilkan',
            'data'             => $campaign
        ];
    }
}
