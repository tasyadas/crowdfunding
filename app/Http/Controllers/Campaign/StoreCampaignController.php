<?php

namespace App\Http\Controllers\Campaign;

use App\Http\Controllers\Controller;
use App\Models\Campaign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;

class StoreCampaignController extends Controller
{
    /**
     * @var ImageManager
     */
    private $image;

    /**
     * ImageUploader constructor.
     * @param ImageManager $image
     */
    public function __construct(ImageManager $image)
    {
        $this->image = $image;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'title'       => 'required',
            'description' => 'required',
            'image'       => 'required|mimes:jpg,jpeg,png'
        ]);

        $campaign = Campaign::create([
            'title'       => $request->title,
            'description' => $request->description
        ]);

        $image          = $request->image;
        $tmpFilePath    = $this->getTempPath($image->getClientOriginalExtension());
        $upload         = $this->image->make($image->getRealPath())->save($tmpFilePath);
        $filePath       = sprintf('campaigns/%s/', $campaign->id) . uniqid() . '.' . $image->getClientOriginalExtension();

        Storage::put('public/'.$filePath, file_get_contents($tmpFilePath));
        $campaign->image = $filePath;
        $upload->destroy();

        $campaign->save();

        return [
            'response_code'    => "00",
            'response_message' => 'Campaign berhasil ditambahkan',
            'data'             => $campaign
        ];
    }

    /**
     * @param string $extension
     *
     * @return string
     */
    private function getTempPath(string $extension)
    {
        return sys_get_temp_dir() . '/' . uniqid() . '.' . $extension;
    }
}
