<?php


namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @author Tashya Dwi Askara Siahaan <tasyadwiaskarasiahaan@gmail.com>
 **/
final class Role extends Model
{
    use HasFactory, UuidTrait;

    protected $table = 'roles';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $hidden = ['id'];
    protected $fillable = [
        'name'
    ];

    public static function getRole($kode)
    {
        return Role::where('name', $kode)->first();
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
