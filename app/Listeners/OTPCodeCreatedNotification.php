<?php


namespace App\Listeners;

use App\Events\OTPCodeCreated;
use App\Mail\OtpCodeCreatedMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

/**
 * @author Tashya Dwi Askara Siahaan <tasyadwiaskarasiahaan@gmail.com>
 **/
class OTPCodeCreatedNotification implements ShouldQueue
{
    /**
     * @param OTPCodeCreated $event
     */
    public function handle(OTPCodeCreated $event)
    {
        Mail::to($event->user->email)->send(new OtpCodeCreatedMail($event->user));
    }
}
