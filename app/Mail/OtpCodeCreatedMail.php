<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OtpCodeCreatedMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;

    /**
     * OtpCodeCreatedMail constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.user-created')
                    ->subject('CrowdFunding OTP Code')
                    ->with('user', $this->user);
    }
}
