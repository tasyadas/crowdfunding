<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('App\Http\Controllers\Auth')->group(function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::group(['middleware' => 'auth:api'], function () {
            Route::post('logout', 'LogoutController');
            Route::post('check-token', 'CheckTokenController');
        });

        Route::post('register', 'RegisterController');
        Route::post('verification', 'VerificationController');
        Route::post('regenarate-otp', 'RegenerateOtpCodeController');
        Route::post('login', 'LoginController');
        Route::post('update-password', 'UpdatePasswordController');

        Route::get('social/{provider}', 'SocialiteController@redirectToProvider');
        Route::get('social/{provider}/callback', 'SocialiteController@handleProviderCallback');
    });
});

Route::namespace('App\Http\Controllers\Profile')->group(function () {
    Route::group(['middleware' => 'auth:api', 'prefix' => 'profile'], function () {
        Route::get('get-profile', 'GetProfileController');
        Route::post('update-profile', 'UpdateProfileController');
    });
});

Route::namespace('App\Http\Controllers\Campaign')->group(function () {
    Route::get('campaigns/{count}', 'PickRandomCampaignController');
    Route::get('campaigns', 'GetAllCampaignController');
    Route::get('campaign/{campaign}', 'ShowCampaignController');
    Route::get('campaign/search/{keyword}', 'SearchCampaignController');

    Route::group(['middleware' => 'auth:api', 'prefix' => 'campaigns'], function () {
        Route::post('/', 'StoreCampaignController');
    });
});

Route::namespace('App\Http\Controllers\Blog')->group(function () {
    Route::get('blogs/{count}', 'PickRandomBlogController');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('blogs', 'StoreBlogController');
    });
});
