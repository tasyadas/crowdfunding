let objectData = [{
    'nama': 'strawberry',
    'warna': 'merah',
    'ada_bijinya': 'tidak',
    'harga': 9000,
},
{
    'nama': 'jeruk',
    'warna': 'oranye',
    'ada_bijinya': 'ada',
    'harga': 8000,
},
{
    'nama': 'Semangka',
    'warna': 'Hijau & Merah',
    'ada_bijinya': 'ada',
    'harga': 10000,
},
{
    'nama': 'Pisang',
    'warna': 'Kuning',
    'ada_bijinya': 'tidak',
    'harga': 5000,
}]

console.log(objectData[0])
